/// Heartbeat helper(s)
use crate::zmqwrappers::TokioPubSubManager;
use crate::TerminationFlag;
// we need to explicitly import this trait for it to work
use datastreamcorelib::pubsub::PubSubManager;
use datastreamcorelib::utils::create_heartbeat_message;
use failure::Fallible;
use std::sync::atomic::Ordering;
use std::time::Duration;
use tokio::time::delay_for;

/// Task that sends heartbeat message every 500ms on the default pub socket.
pub async fn heartbeat_task(term: TerminationFlag) -> Fallible<()> {
    let psmgr = TokioPubSubManager::instance();
    loop {
        // Scope limit the lock to this critical part
        {
            match create_heartbeat_message() {
                Ok(msg) => {
                    log::trace!("Sending heartbeat");
                    psmgr.lock().publish(&msg)?;
                }
                Err(e) => {
                    log::error!("Could not create HB message: {}", e);
                }
            };
        }
        if term.load(Ordering::Relaxed) {
            log::trace!("Got term flag, exiting heartbeat task");
            return Ok(());
        }
        delay_for(Duration::from_millis(500)).await;
    }
}
