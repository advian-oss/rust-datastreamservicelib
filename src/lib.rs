pub mod heartbeat;
pub mod utils;
pub mod zmqwrappers;
use std::sync::atomic::AtomicBool;
use std::sync::Arc;

/// Arc to Atomicbool, used to signal tasks that they should gracefully terminate
pub type TerminationFlag = Arc<AtomicBool>;

#[cfg(test)]
mod tests {
    #[test]
    fn it_works() {
        assert_eq!(2 + 2, 4);
    }
}
