#!/bin/bash -l
set -e
if [ "$#" -eq 0 ]; then
  echo "NOTHING TO DO!"
  exit 1
else
  exec "$@"
fi
