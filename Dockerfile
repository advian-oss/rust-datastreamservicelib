# syntax=docker/dockerfile:1.1.7-experimental
##################################
# Base image for building things #
##################################
FROM rust:bullseye as builder_base

RUN apt-get update && apt-get install -y \
        curl \
        git \
        bash \
        build-essential \
        libffi-dev \
        libzmq5 \
        openssl \
        librust-openssl-dev \
        tini \
        openssh-client \
        python3-pip \
        python3-ruamel.yaml \
        python3-dev \
        libzmq3-dev \
    && rm -rf /var/lib/apt/lists/* && apt-get clean \
    # githublab ssh
    && mkdir -p -m 0700 ~/.ssh && ssh-keyscan gitlab.com github.com | sort > ~/.ssh/known_hosts \
    && pip3 install --ignore-installed pre-commit \
    && echo 'export PATH=/usr/local/cargo/bin:$PATH' >>$HOME/.profile \
    && true

WORKDIR /app
# Copy just the manifest so we can get deps
COPY ./Cargo.toml ./Cargo.lock /app/
# Make a dummy file in src so we can compile deps
RUN --mount=type=ssh mkdir src \
    && echo "pub fn main() {println!(\"if you see this, the build broke\")}" > src/lib.rs \
    # Compile deps both devel and release, then delete the dummy main executable
    && cargo fetch \
    && cargo build \
    && cargo build --release \
    && rm -f target/debug/deps/*datastreamservicelib* \
    && rm -f target/debug/*datastreamservicelib* \
    && rm -f target/release/deps/*datastreamservicelib* \
    && rm -f target/release/*datastreamservicelib* \
    && true


#####################################
# Base stage for development builds #
#####################################
FROM builder_base as devel_build
# Copy everything to the image
COPY . /app
WORKDIR /app
# Add rustfmt, we are going to need it.
RUN --mount=type=ssh rustup component add rustfmt \
    && cargo install cargo2junit cargo-tarpaulin \
    && true

#############
# Run tests #
#############
FROM devel_build as test
RUN --mount=type=ssh cargo fetch \
    && cargo build \
    && docker/pre_commit_init.sh \
    && true
ENTRYPOINT ["/usr/bin/tini", "--", "docker/entrypoint-test.sh"]


###########
# Hacking #
###########
FROM devel_build as devel_shell
RUN apt-get update && apt-get install -y zsh \
    && rm -rf /var/lib/apt/lists/* && apt-get clean \
    && sh -c "$(curl -fsSL https://raw.githubusercontent.com/ohmyzsh/ohmyzsh/master/tools/install.sh)" \
    && echo "source /root/.profile" >>/root/.zshrc \
    && true
ENTRYPOINT ["/bin/zsh", "-l"]
