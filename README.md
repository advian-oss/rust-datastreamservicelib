# rust-datastreamservicelib

Rust version of https://gitlab.com/advian-oss/python-datastreamservicelib, using Tokio

NOTE: 1.0 is API incompatible with <1.0 versions due to starting to use
parking_lot mutexes for synchronization.

## Docker

Remember to add any new system packages you install via the devel\_shell
to the Dockerfile too. Easy way to check if you remembered everything is
to rebuild the test image and run it (see below).

### SSH agent forwarding

We need [buildkit](https://docs.docker.com/develop/develop-images/build_enhancements/):

### Creating the container

Build image, create container and start it:

    docker build --ssh default --target devel_shell -t rustdatastreamservicelib:devel_shell .
    docker create --name rustdatastreamservicelib_devel -v `pwd`":/app" -it rustdatastreamservicelib:devel_shell
    docker start -i rustdatastreamservicelib_devel

This will give you a shell with system level dependencies installed, you
should do any shell things (like run tests, pre-commit checks etc)
there.

### pre-commit considerations

If working in Docker instead of native env you need to run the
pre-commit checks in docker too:

    docker exec -i rustdatastreamservicelib_devel /bin/bash -c "pre-commit install"
    docker exec -i rustdatastreamservicelib_devel /bin/bash -c "pre-commit run --all-files"

You need to have the container running, see above. Or alternatively use
the docker run syntax but using the running container is faster:

    docker run --rm -v `pwd`":/app" rustdatastreamservicelib:devel_shell -c "pre-commit run --all-files"

### Test suite

You can use the devel shell to run py.test when doing development, for
CI use the \"test\" target in the Dockerfile:

    docker build --ssh default --target test -t rustdatastreamservicelib:test .
    docker run --rm --security-opt seccomp=unconfined -v `pwd`":/app" rustdatastreamservicelib:test
